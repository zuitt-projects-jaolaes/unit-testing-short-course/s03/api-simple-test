const express = require('express');
const app = express();
const PORT = 5000;

app.use(express.json());

require('./routes/userRoutes')(app, {}); //required userRoutes and send the app and empty object as its argument.

app.listen(PORT, ()=> {
	console.log(`Listening to PORT ${PORT}` );
})