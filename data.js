const user = {
	"Brandon": {
		"fullName": "Brandon Boyd",
		"age": 35
	},
	"Steve": {
		"fullName": "Steve Tyler",
		"age": 56
	} 
}

module.exports = {
	user: user
}